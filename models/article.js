'use strict'

let mongoose = require('mongoose');
let schema = mongoose.Schema;

let ArticleSchema = schema({
  title: String,
  content: String,
  date: { type: Date, default: Date.now },
  image: String
});

module.exports = mongoose.model('Article', ArticleSchema);
