'use strict'

var validator = require('validator');
var Article = require('../models/article');

var fs = require('fs');
var path = require('path');

var controller = {
  datosCurso: (req, res) => {

    var hola = req.body.hola;

    return res.status(200).send({
      curso: 'Master en Frameworks JS',
      estudiante: 'Alfredo Rentería Villanueva',
      year: '2020',
      hola
    });
  },
  test: (req, res) => {
    return res.status(200).send({
      message: 'Soy la acción Test de mi controlador de Artículos'
    });
  },
  save: (req, res) => {
    // Recoger los parámetros por POST
    var params = req.body;

    // Validar datos (validator)
    try {
      var validateTitle = !validator.isEmpty(params.title);
      var validateContent = !validator.isEmpty(params.content);
    } catch (error) {
      return res.status(200).send({
        status: 'error',
        message: 'Faltan datos por enviar.'
      })
    }

    if(validateTitle && validateContent){
      // Crear el objeto a guardar
      var article = new Article();

      // Asignar valores
      article.title = params.title;
      article.content = params.content;
      article.image = null;

      // Guardar el artículo
      article.save((err, storedArticle) => {

        if(err || !storedArticle){
          // Devolver error
          return res.status(404).send({
            status: 'error',
            message: 'El artículo no se ha guardado.'
          })
        }

        // Devolver respuesta
        return res.status(200).send({
          status: 'success',
          article: storedArticle
        });
      });
    }else{
      return res.status(200).send({
        status: 'error',
        message: 'Los datos no son válidos.'
      })
    }
  },
  getArticles: (req, res) => {

    var last = req.params.last;
    var query = Article.find({});

    if(last || last != undefined){
      query.limit(5);
    }

    // Find y acomoda del más reciente al más viejo utilizando "-_id"
    query.sort('-_id').exec((err, articles) => {

      if(err){
        return res.status(500).send({
          status: 'error',
          message: 'Error al devolver los datos.'
        })
      }

      if(!articles ){
        return res.status(404).send({
          status: 'error',
          message: 'No hay artículos para mostrar.'
        })
      }

      return res.status(200).send({
        status: 'success',
        articles
      })
    });
  },
  getArticle: (req, res) => {

    // Recoger el ID de la URL
    var articleId = req.params.id;

    // Comprobar que existe
    if(!articleId || articleId == null){
      return res.status(404).send({
        status: 'error',
        message: 'No existe el artículo.'
      })
    }

    // Buscar el artículo
    Article.findById(articleId, (err, article) => {
      if(err || !article){
        return res.status(404).send({
          status: 'error',
          message: 'No existe el artículo.'
        })
      }

      // Devolverlo en json
      return res.status(200).send({
        status: 'success',
        article
      })
    });
  },
  update: (req, res) => {

    // Recoger el ID de la URL
    var articleId = req.params.id;

    // Recoger los datos que llegan por PUT
    var params = req.body;

    // Validar los datos
    try {
      var validateTitle = !validator.isEmpty(params.title);
      var validateContent = !validator.isEmpty(params.content);

    } catch (error) {
      return res.status(200).send({
        status: 'error',
        message: 'Faltan datos por enviar'
      })
    }

    if(validateTitle && validateContent){
      // Find and update
      Article.findOneAndUpdate({_id: articleId}, params, {new: true}, (err, articleUpdated) => {
        if(err){
          return res.status(500).send({
            status: 'error',
            message: 'Error al actualizar'
          })
        }
        if(!articleUpdated){
          return res.status(404).send({
            status: 'error',
            message: 'No existe el artículo'
          })
        }
        return res.status(200).send({
          status: 'success',
          article: articleUpdated
        })
      });
    }else{
      return res.status(200).send({
        status: 'error',
        message: 'La validación no es correcta!'
      })
    }
  },
  delete: (req, res) => {

    // Recoger el ID de la URL
    var articleId = req.params.id;

    // Find and delete
    Article.findOneAndDelete({_id: articleId}, (err, articleRemoved) => {
      if(err){
        return res.status(500).send({
          status: 'error',
          message: 'Error al borrar!'
        })
      }
      if(!articleRemoved){
        return res.status(404).send({
          status: 'error',
          message: 'El artículo a borrar no existe'
        })
      }

      return res.status(200).send({
        status: 'success',
        article: articleRemoved
      })
    });
  },
  upload: (req, res) => {
    // Configurar el módulo Connect-Multiparty. Esto se hace en el router/article.js (hecho)

    // Recoger el fichero de la petición
    var fileName = 'Imagen no subida';

    if(!req.files){
      return res.status(404).send({
        status: 'error',
        message: fileName
      })
    }

    // Conseguir el nombre y la extensión del archivo
    let filePath = req.files.file0.path;
    let fileSplit = filePath.split('/');
    /*
      ADVERTENCIA: En Linux o MacOS se utiliza '/', en Windows: "\\"
    */
    // Nombre del archivo
    fileName = fileSplit[2];

    // Extensión del fichero
    let extensionSplit = fileName.split('\.');
    let extension = extensionSplit[1];

    // Comprobar la extensión (Sólo imágenes, si no es imagen, borrar el fichero)
    if(extension != 'png' && extension != 'jpeg' && extension != 'jpg' && extension != 'gif'){
      // Borrar el archivo subido
      fs.unlink(filePath, (err) => {
        return res.status(200).send({
          status: 'error',
          message: 'La extensión de la imagen no es válida.'
        });
      });

    }else{
      // Si todo es válido, buscar el artículo, asignarle el nombre de la imagen y actualizarlo

      // Recoger el ID de la URL
      var articleId = req.params.id;

      Article.findOneAndUpdate({_id: articleId}, {image: fileName}, {new: true}, (err, articleUpdated) => {

        if(err || !articleUpdated){
          return res.status(500).send({
            status: 'error',
            message: 'Error al guardar la imagen.'
          });
        }

        return res.status(200).send({
          status: 'success',
          article: articleUpdated
        });
      });
    }
  },
  getImage: (req, res) => {

    let file = req.params.image;
    var pathFile = './upload/articles/' + file;

    fs.exists(pathFile, (exists) => {
      if(exists){
        return res.sendFile(path.resolve(pathFile));
      }else{
        return res.status(404).send({
          status: 'error',
          message: 'La imagen no existe'
        });
      }
    });
  },
  search: (req, res) => {

    // Obtener el string a buscar
    var searchString = req.params.search;

    // Find or
    Article.find({ "$or": [
      { "title": { "$regex": searchString, "$options": "i" } },
      { "content": { "$regex": searchString, "$options": "i" } }
    ]})
    .sort([['date', 'descending']])
    .exec((err, articles) => {

      if(err){
        return res.status(500).send({
          status: 'error',
          message: 'Error en la petición'
        });
      }
      if(!articles || articles.length <= 0){
        return res.status(404).send({
          status: 'error',
          message: 'No se encontraron resultados para la búsqueda.'
        });
      }

      return res.status(200).send({
        status: 'success',
        articles
      });
    });



  }
};

module.exports = controller;