'use strict'

// Cargar módulos de Node para crear el Server
let express = require('express');
let bodyParser = require('body-parser');

// Ejecutar Express (para poder trabajar con http)
let app = express();

// Cargar ficheros/rutas
let ArticleRoutes = require('./routes/article');

// Cargar middlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Activar el CORS
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
  next();
});


// Añadir rutas / prefijos a las rutas
app.use(ArticleRoutes);

// Ruta o método de prueba para el API REST
/*
app.post('/datos-curso',(req, res) => {

  let hola = req.body.hola;

  return res.status(200).send({
    curso: 'Master en Frameworks JS',
    estudiante: 'Alfredo Rentería Villanueva',
    year: '2020',
    hola
  });
});
*/

// Exportar el módulo (fichero actual) para poder usar este archivo en index.js
module.exports = app;